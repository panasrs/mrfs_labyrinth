// Fill out your copyright notice in the Description page of Project Settings.


#include "AI_Character.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Vitality/VitalityComponent.h"
#include "Weapons/WeaponBase.h"
#include "Components/SceneComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "Perception/AIPerceptionComponent.h"
#include "Perception/AISenseConfig_Sight.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/Pawn.h"
#include "Animation/AnimMontage.h"
#include "Labyrinth_AIController.h"

// Sets default values
AAI_Character::AAI_Character()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	GetCharacterMovement()->bOrientRotationToMovement = false;
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 600.0f, 0.0f);

	VitalityComp = CreateDefaultSubobject<UVitalityComponent>(TEXT("VitalityComp"));
		
}

// Called when the game starts or when spawned
void AAI_Character::BeginPlay()
{
	Super::BeginPlay();
	
	CurrentWeapon = SpawnWeapon();
	AttachWeapon(WeaponSocket);

	VitalityComp->OnHealthChanged.Broadcast();
}

void AAI_Character::DeadAI()
{
	GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	GetController()->UnPossess();
}

void AAI_Character::StartFire()
{
	if (AIFireMontage != NULL)
	{
		PlayAnimMontage(AIFireMontage);
	}

	if (CurrentWeapon && bCanFire)
	{
		CurrentWeapon->StartFire();
	}

}

void AAI_Character::StopFire()
{
	if (CurrentWeapon)
	{
		CurrentWeapon->StopFire();
	}
	
}

void AAI_Character::AttachWeapon(FName SocketName)
{
	if (CurrentWeapon)
	{
		CurrentWeapon->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetIncludingScale, SocketName);
	}
}

class AWeaponBase* AAI_Character::SpawnWeapon()
{
	if (WeaponToSpawn_Class)
	{
		FTransform TmpTransform = FTransform::Identity;
		FActorSpawnParameters SpawnParams;
		SpawnParams.Owner = this;

		UE_LOG(LogTemp, Warning, TEXT("Trying to spawn weapon"));
		return Cast<AWeaponBase>(GetWorld()->SpawnActor(WeaponToSpawn_Class, &TmpTransform, SpawnParams));
	}

	return nullptr;
}

// Called every frame
void AAI_Character::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AAI_Character::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

