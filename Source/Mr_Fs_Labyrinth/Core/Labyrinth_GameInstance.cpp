// Fill out your copyright notice in the Description page of Project Settings.


#include "Labyrinth_GameInstance.h"

int ULabyrinth_GameInstance::GetCurrentScore() const
{
	return CurrentScore;
}

void ULabyrinth_GameInstance::AddScore(int ScoreToAdd)
{
	CurrentScore += ScoreToAdd;
}

void ULabyrinth_GameInstance::FlushCurrentScore()
{
	CurrentScore = 0;
}
