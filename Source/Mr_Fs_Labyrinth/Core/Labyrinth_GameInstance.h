// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "Labyrinth_GameInstance.generated.h"

/**
 * 
 */
UCLASS()
class MR_FS_LABYRINTH_API ULabyrinth_GameInstance : public UGameInstance
{
	GENERATED_BODY()
	
private:
	int CurrentScore = 0;

public:

	UFUNCTION(BlueprintCallable)
		int GetCurrentScore() const;

	UFUNCTION(BlueprintCallable)
	void AddScore(int ScoreToAdd);

	UFUNCTION(BlueprintCallable)
	void FlushCurrentScore();
	
};
