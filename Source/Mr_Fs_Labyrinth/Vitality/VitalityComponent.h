// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "VitalityComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FNoParamDelegate);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class MR_FS_LABYRINTH_API UVitalityComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UVitalityComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	UFUNCTION()
		float GetMaxHealth();

	UFUNCTION()
		float GetCurrentHealth();

	UFUNCTION(BlueprintCallable)
		void SetCurrentHealth(float Health);

	UFUNCTION(BlueprintCallable)
		void AddHealth(float HealthToAdd);
	
	UFUNCTION()
		void DamageHandle(AActor* DamagedActor, float Damage, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser);

	UFUNCTION()
		void RegenHealth();

	UFUNCTION()
		void StopRegenHealth();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health")
		float MaxHealth = 150;

	UPROPERTY(BlueprintReadOnly, Category = "Health")
		float CurrentHealth = MaxHealth;

	//Enable RegenHP
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health")
		bool bCanRegenHealth = true;

	//HP to Regen
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health", meta = (EditCondition = "bCanRegenHealth"))
		float RegenHealthRate = 5;

	//Starts RegenHP in N sec since damaged
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health", meta = (EditCondition = "bCanRegenHealth"))
		float RegenHealthDelay = 2;

	FTimerHandle AutoRegenHealth_TH;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Score")
		int Score = 100;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION(BlueprintCallable)
		bool IsAlive();

	UPROPERTY(BlueprintAssignable, Category = "Delegates")
		FNoParamDelegate OnHealthChanged;

	UPROPERTY(BlueprintAssignable, Category = "Delegates")
		FNoParamDelegate OnOwnerDied;

};
