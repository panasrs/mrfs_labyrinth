// Fill out your copyright notice in the Description page of Project Settings.


#include "WeaponProjectile.h"
#include "ProjectileBase.h"
#include "ImpactEffectComponent.h"
#include <GameFramework/Character.h>

void AWeaponProjectile::Fire()
{
	Super::Fire();

	if (ProjectileClass)
	{
		FTransform TmpTransform = GetMuzzleTransform();

		AProjectileBase* TmpProjectile = Cast<AProjectileBase>(GetWorld()->SpawnActor(ProjectileClass, &TmpTransform));

		TmpProjectile->OnHitFire.BindDynamic(ImpactEffectComponent, &UImpactEffectComponent::SpawnImpactEffect);
	}
}

void AWeaponProjectile::Reload()
{
	Super::Reload();
}
