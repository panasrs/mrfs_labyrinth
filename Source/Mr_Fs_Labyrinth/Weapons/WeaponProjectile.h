// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Weapons/WeaponBase.h"
#include "WeaponProjectile.generated.h"

/**
 * 
 */
UCLASS(abstract)
class MR_FS_LABYRINTH_API AWeaponProjectile : public AWeaponBase
{
	GENERATED_BODY()
	
protected:
	virtual void Fire() override;
	
	virtual void Reload() override;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<class AProjectileBase> ProjectileClass;

};
